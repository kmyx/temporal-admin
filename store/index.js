export const state = () => ({
  menu: [
    {
      name: 'Contenido', slug: 'section', img: '/nav/section.svg', childs: [
        { name: 'Idiomas', slug: 'home', href: '/dashboard/language' },
        { name: 'Inicio', slug: 'home', href: '/dashboard/page/page' },
        // { name: 'Nosotros', slug: 'nosotros', href: '/dashboard/page/nosotros' },
        // { name: 'Servicios', slug: 'service', href: '/dashboard/page/servicios' },
        // { name: 'Proyectos', slug: 'portrait', href: '/dashboard/page/projects' },
        // { name: 'Contacto', slug: 'contacto', href: '/dashboard/page/contacto' },
      ],
    },
    { name: 'Registro de Contacto', slug: 'contacto', img: '/nav/message.svg', href: '/dashboard/contact' },
    { name: 'Áreas de práctica', slug: 'practiceArea', img: '/nav/project.svg', href: '/dashboard/areaPractice' },
    { name: 'Clientes', slug: 'customer', img: '/nav/service.svg', href: '/dashboard/customers' },
    { name: 'Idiomas', slug: 'language', img: '/nav/email.svg', href: '/dashboard/language' },
    { name: 'Staff de Abogados', slug: 'staff', img: '/nav/lawyer.svg', href: '/dashboard/staffLawyer' },
    { name: 'Información General', slug: 'information', img: '/nav/information.svg', href: '/dashboard/infogeneral' }
  ]
})

export const mutations = {
  UPDATE_TITLE(state, title) {
    state.title = title
  }
}

export const actions = {
  setTitle({ commit }, title) {
    commit('UPDATE_TITLE', title)
  },
}